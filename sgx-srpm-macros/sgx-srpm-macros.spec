Name: sgx-srpm-macros
Version: 1
Release: 1%{?dist}
License: MIT-0
Summary: Source RPM macros for working with the SGX SDK

Source0: macros.sgx-srpm

BuildArch: noarch

%description
RPM macros for working with the SGX SDK

%install
%__install -d %{buildroot}/%{_rpmmacrodir}/
cp %{SOURCE0} %{buildroot}/%{_rpmmacrodir}/

%files
%{_rpmmacrodir}/macros.sgx-srpm

%changelog
* Mon Jan 29 2024 Daniel P. Berrangé <berrange@redhat.com> - 1-1
- Initial package
