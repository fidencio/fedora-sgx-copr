From f964d9ba8184554568b5f9ebe06a08d239913e8b Mon Sep 17 00:00:00 2001
From: =?UTF-8?q?C=C3=A9dric=20Le=20Goater?= <clg@redhat.com>
Date: Wed, 12 Jul 2023 17:46:57 +0200
Subject: migration: Add switchover ack capability
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit

RH-Author: Cédric Le Goater <clg@redhat.com>
RH-MergeRequest: 179: vfio: live migration support
RH-Bugzilla: 2192818
RH-Acked-by: Eric Auger <eric.auger@redhat.com>
RH-Acked-by: Miroslav Rezanina <mrezanin@redhat.com>
RH-Commit: [8/28] 2f4ca020783bd617eca13b18289fce764279833b (clegoate/qemu-kvm-c9s)

Bugzilla: https://bugzilla.redhat.com/2192818

commit 6574232fff6a
Author: Avihai Horon <avihaih@nvidia.com>
Date:   Wed Jun 21 14:11:54 2023 +0300

    migration: Add switchover ack capability

    Migration downtime estimation is calculated based on bandwidth and
    remaining migration data. This assumes that loading of migration data in
    the destination takes a negligible amount of time and that downtime
    depends only on network speed.

    While this may be true for RAM, it's not necessarily true for other
    migrated devices. For example, loading the data of a VFIO device in the
    destination might require from the device to allocate resources, prepare
    internal data structures and so on. These operations can take a
    significant amount of time which can increase migration downtime.

    This patch adds a new capability "switchover ack" that prevents the
    source from stopping the VM and completing the migration until an ACK
    is received from the destination that it's OK to do so.

    This can be used by migrated devices in various ways to reduce downtime.
    For example, a device can send initial precopy metadata to pre-allocate
    resources in the destination and use this capability to make sure that
    the pre-allocation is completed before the source VM is stopped, so it
    will have full effect.

    This new capability relies on the return path capability to communicate
    from the destination back to the source.

    The actual implementation of the capability will be added in the
    following patches.

    Signed-off-by: Avihai Horon <avihaih@nvidia.com>
    Reviewed-by: Peter Xu <peterx@redhat.com>
    Acked-by: Markus Armbruster <armbru@redhat.com>
    Tested-by: YangHang Liu <yanghliu@redhat.com>
    Acked-by: Alex Williamson <alex.williamson@redhat.com>
    Signed-off-by: Cédric Le Goater <clg@redhat.com>

Conflicts:
    - qapi/migration.json
      re-indent of @switchover-ack to avoid ../qapi/migration.json:482:1:
      unexpected de-indent (expected at least 17 spaces)

Signed-off-by: Cédric Le Goater <clg@redhat.com>

Patch-name: kvm-migration-Add-switchover-ack-capability.patch
Patch-id: 120
Patch-present-in-specfile: True
---
 migration/options.c | 21 +++++++++++++++++++++
 migration/options.h |  1 +
 qapi/migration.json | 14 +++++++++++++-
 3 files changed, 35 insertions(+), 1 deletion(-)

diff --git a/migration/options.c b/migration/options.c
index a76984276d..c3df6c6dde 100644
--- a/migration/options.c
+++ b/migration/options.c
@@ -182,6 +182,8 @@ Property migration_properties[] = {
     DEFINE_PROP_MIG_CAP("x-zero-copy-send",
             MIGRATION_CAPABILITY_ZERO_COPY_SEND),
 #endif
+    DEFINE_PROP_MIG_CAP("x-switchover-ack",
+                        MIGRATION_CAPABILITY_SWITCHOVER_ACK),
 
     DEFINE_PROP_END_OF_LIST(),
 };
@@ -305,6 +307,13 @@ bool migrate_return_path(void)
     return s->capabilities[MIGRATION_CAPABILITY_RETURN_PATH];
 }
 
+bool migrate_switchover_ack(void)
+{
+    MigrationState *s = migrate_get_current();
+
+    return s->capabilities[MIGRATION_CAPABILITY_SWITCHOVER_ACK];
+}
+
 bool migrate_validate_uuid(void)
 {
     MigrationState *s = migrate_get_current();
@@ -532,6 +541,18 @@ bool migrate_caps_check(bool *old_caps, bool *new_caps, Error **errp)
         }
     }
 
+    if (new_caps[MIGRATION_CAPABILITY_SWITCHOVER_ACK]) {
+        if (!new_caps[MIGRATION_CAPABILITY_RETURN_PATH]) {
+            error_setg(errp, "Capability 'switchover-ack' requires capability "
+                             "'return-path'");
+            return false;
+        }
+
+        /* Disable this capability until it's implemented */
+        error_setg(errp, "'switchover-ack' is not implemented yet");
+        return false;
+    }
+
     return true;
 }
 
diff --git a/migration/options.h b/migration/options.h
index 7b0f7245ad..0fc7be6869 100644
--- a/migration/options.h
+++ b/migration/options.h
@@ -47,6 +47,7 @@ bool migrate_postcopy_ram(void);
 bool migrate_rdma_pin_all(void);
 bool migrate_release_ram(void);
 bool migrate_return_path(void);
+bool migrate_switchover_ack(void);
 bool migrate_validate_uuid(void);
 bool migrate_xbzrle(void);
 bool migrate_zero_blocks(void);
diff --git a/qapi/migration.json b/qapi/migration.json
index 2c35b7b9cf..b6a58347cc 100644
--- a/qapi/migration.json
+++ b/qapi/migration.json
@@ -478,6 +478,18 @@
 #                    should not affect the correctness of postcopy migration.
 #                    (since 7.1)
 #
+# @switchover-ack: If enabled, migration will not stop the source VM
+#                  and complete the migration until an ACK is received
+#                  from the destination that it's OK to do so.
+#                  Exactly when this ACK is sent depends on the
+#                  migrated devices that use this feature.  For
+#                  example, a device can use it to make sure some of
+#                  its data is sent and loaded in the destination
+#                  before doing switchover.  This can reduce downtime
+#                  if devices that support this capability are
+#                  present.  'return-path' capability must be enabled
+#                  to use it.  (since 8.1)
+#
 # Features:
 # @unstable: Members @x-colo and @x-ignore-shared are experimental.
 #
@@ -492,7 +504,7 @@
            'dirty-bitmaps', 'postcopy-blocktime', 'late-block-activate',
            { 'name': 'x-ignore-shared', 'features': [ 'unstable' ] },
            'validate-uuid', 'background-snapshot',
-           'zero-copy-send', 'postcopy-preempt'] }
+           'zero-copy-send', 'postcopy-preempt', 'switchover-ack'] }
 
 ##
 # @MigrationCapabilityStatus:
-- 
2.41.0

