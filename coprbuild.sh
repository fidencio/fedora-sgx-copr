#!/bin/sh

set -e
set -v

name=$1

case ${name} in
    *:bootstrap)
	dir=${name}
	name=$(echo ${name} | sed -e 's/:bootstrap//')
	;;
    *)
	dir=${name}
	;;
esac

copr-cli buildscm --method make_srpm --clone-url https://gitlab.com/berrange/fedora-sgx-copr --spec ${dir}/${name}.spec sgx
