#!/bin/sh

set -e
set -v

name=$1

case ${name} in
    *:bootstrap)
	dir=${name}
	name=$(echo ${name} | sed -e 's/:bootstrap//')
	;;
    *)
	dir=${name}
	;;
esac

mkdir -p build
rm -f build/*.src.rpm

make -f .copr/Makefile srpm spec=${dir}/${name}.spec outdir=build srpm

rpmbuild --rebuild build/*.src.rpm
