
# If set to '1' then merely create a sgx-enclave-dcap-devel
# containing header files
%{!?sgx_bootstrap:%define sgx_bootstrap 0}

# We need direct control over build flags used for the
# SGX enclave code.
#
# Unfortunately this package also builds some native
# code, but he makefiles for native & enclave code
# are so horribly tangled it is difficult to make
# native code honour default RPM CFLAGS while keeping
# those flags out of enclave code
%undefine _auto_set_build_flags

%if %{sgx_bootstrap} == 1
%define debug_package %{nil}
%endif

%global psw_version 2.22

Name: sgx-dcap
Version: 1.19
Release: 1%{?dist}
Summary: SGX Data Center Attestation Primitives

License: BSD-3-Clause
URL: https://github.com/intel/SGXDataCenterAttestationPrimitives

Source0: https://github.com/intel/SGXDataCenterAttestationPrimitives/archive/refs/tags/dcap_%{version}_reproducible.tar.gz

Source1: https://download.01.org/intel-sgx/sgx-dcap/%{version}/linux/prebuilt_dcap_%{version}.tar.gz

# The .service files bundled with the src tarball have a bunch
# of cruft set that is not applicable or simply wrong. Rather
# than trying to edit them with sed, we just provide new complete
# service files here.
Source2: pccs.sysusers.conf
Source3: pccs.service
Source4: qgs.sysusers.conf
Source5: qgs.service
Source6: mpa_registration.service

Source7: https://people.redhat.com/berrange/dcap-%{version}-pccs-node_modules.tar.xz
Source8: pccs-ssl.sh

Patch1: 0001-Don-t-include-global-buildenv.mk.patch
Patch2: 0002-Fix-compat-with-latest-stdc-libraries.patch
Patch3: 0003-Add-missing-include-directories.patch
Patch4: 0004-Drop-use-of-bundled-pre-built-openssl.patch
Patch5: 0005-Improve-debuggability-of-build-system.patch
Patch6: 0006-Use-distro-provided-rapidjson-package.patch
Patch7: 0007-Load-enclaves-from-the-library-directory.patch
Patch8: 0008-Sanitize-paths-to-all-resources-in-PCCS-server.patch
Patch9: 0009-Switch-default-PCCS-port-number-from-8081-to-10801.patch
Patch10: 0010-Look-for-versioned-sgx_urts-library-in-PCKRetrievalT.patch
Patch11: 0011-Don-t-import-pypac-in-pccsadmin.patch
Patch12: 0012-Look-for-PCKRetrievalTool-config-file-in-etc.patch

BuildRequires: sgx-srpm-macros
%if %{sgx_bootstrap} == 0
BuildRequires: sgx-enclave-devel
BuildRequires: sgx-enclave-ssl-devel
BuildRequires: sgx-enclave-rapidjson-devel
BuildRequires: rapidjson-devel
BuildRequires: sgx-platform-stubs-devel
%endif

BuildRequires: autoconf
BuildRequires: automake
BuildRequires: binutils
BuildRequires: libtool
BuildRequires: gcc
BuildRequires: gcc-c++
BuildRequires: make
BuildRequires: cmake
BuildRequires: ocaml
BuildRequires: ocaml-ocamlbuild
BuildRequires: openssl-devel
BuildRequires: protobuf-compiler
BuildRequires: protobuf-devel
BuildRequires: libcurl-devel
BuildRequires: python3-devel
#BuildRequires: perl-generators
#BuildRequires: perl-interpreter
#BuildRequires: perl-devel
#BuildRequires: perl(FindBin)
#BuildRequires: perl(lib)
#BuildRequires: perl(IPC::Cmd)
#BuildRequires: nasm
BuildRequires: python-unversioned-command
BuildRequires: boost-devel
BuildRequires: systemd-rpm-macros

# SGX is a feature supported and verified on x86_64 only.
ExclusiveArch: x86_64

%description
Intel(R) Software Guard Extensions (Intel(R) SGX) Data Center
Attestation Primitives (Intel(R) SGX DCAP) provides SGX
attestation support targeted for data centers, cloud services
providers and enterprises. This attestation model leverages
Elliptic Curve Digital Signature algorithm (ECDSA) versus
the current client based SGX attestation model which is EPID
based (Enhanced Privacy Identification).


%package -n sgx-platform-dcap-libs
Summary: SGX DCAP platform libraries

%description -n sgx-platform-dcap-libs
SGX DCAP platform libraries


%package -n sgx-platform-dcap-devel
Summary: SGX platform DCAP libraries development

%description -n sgx-platform-dcap-devel
SGX platform DCAP libraries development


%package -n sgx-platform-pccs
Summary: SGX Provisioning Certificate Caching Service
Requires: nodejs
Requires: sgx-platform-mp-ra = %{version}-%{release}

%description -n sgx-platform-pccs
SGX Provisioning Certificate Caching Service


%package -n sgx-platform-pccs-admin
Summary: SGX Provisioning Certificate Caching Service Admin Tool
Requires: sgx-platform-pccs = %{version}-%{release}
Requires: python3-asn1
Requires: python3-pyOpenSSL
Requires: python3-cryptography
Requires: python3-keyring
Requires: python3-requests
Requires: python3-urllib3

%description -n sgx-platform-pccs-admin
SGX Provisioning Certificate Caching Service Admin Tool


%package -n sgx-platform-pck-tool
Summary: SGX PCK Cert ID Retrieval Tool
Requires: sgx-platform-libs = %{psw_version}
Requires: sgx-platform-mp-libs = %{version}-%{release}
Requires: sgx-enclave(id_enclave:signed:intel) = %{version}
Requires: sgx-enclave(pce:signed:intel) >= %{psw_version}

%description -n sgx-platform-pck-tool
SGX PCK Cert ID Retrieval Tool


%package -n sgx-platform-mp-ra
Summary: SGX Multi-package Registration Agent

%description -n sgx-platform-mp-ra
SGX Multi-package Registration Agent


%package -n sgx-platform-mp-libs
Summary: SGX Multi-package libraries

%description -n sgx-platform-mp-libs
SGX Multi-package libraries


%package -n sgx-enclave-dcap-devel
Summary: SGX DCAP enclave development files

%if %{sgx_bootstrap} == 0
Requires: sgx-enclave-devel
%endif

%description -n sgx-enclave-dcap-devel
Summary: SGX DCAP enclave support libraries



%package -n sgx-enclave-dcap-unsigned
Summary: SGX DCAP unsigned enclaves

Provides: sgx-enclave(qe3:unsigned) = %{version}
Provides: sgx-enclave(qve:unsigned) = %{version}
Provides: sgx-enclave(id_enclave:unsigned) = %{version}

%description -n sgx-enclave-dcap-unsigned
SGX DCAP unsigned enclaves


%package -n sgx-enclave-dcap-signed
Summary: SGX DCAP unsigned enclaves

Provides: sgx-enclave(qe3:signed:intel) = %{version}
Provides: sgx-enclave(qve:signed:intel) = %{version}
Provides: sgx-enclave(id_enclave:signed:intel) = %{version}

%description -n sgx-enclave-dcap-signed
SGX DCAP unsigned enclaves


%package -n tdx-guest-libs
Summary: TDX guest libraries

%description -n tdx-guest-libs
TDX guest libraries

This assists guest applications in attesting
their virtual machine environment.


%package -n tdx-platform-qgs
Summary: TDX Quoting Generation Service

Requires: sgx-enclave(tdqe:signed:intel) >= %{version}
Requires: sgx-enclave(pce:signed:intel) >= %{psw_version}
Requires: sgx-enclave(id_enclave:signed:intel) >= %{version}

Requires: sgx-platform-pccs = %{version}-%{release}

%description -n tdx-platform-qgs
TDX Quoting Generation Service


%package -n tdx-platform-libs
Summary: TDX platform libraries

%description -n tdx-platform-libs
TDX platform libraries


%package -n tdx-platform-devel
Summary: TDX platform libraries development

%description -n tdx-platform-devel
TDX platform libraries development


%package -n tdx-enclave-unsigned
Summary: TDX DCAP unsigned enclaves

Provides: sgx-enclave(tdqe:unsigned)

%description -n tdx-enclave-unsigned
TDX DCAP unsigned enclaves


%package -n tdx-enclave-signed
Summary: TDX DCAP unsigned enclaves

Provides: sgx-enclave(tdqe:signed:intel)

%description -n tdx-enclave-signed
TDX DCAP unsigned enclaves


%package -n tdx-guest-devel
Summary: TDX guest libraries development

%description -n tdx-guest-devel
TDX guest libraries development

This enables integration of support for attestation
in applications


%prep
%autosetup -p1 -n SGXDataCenterAttestationPrimitives-dcap_%{version}_reproducible

(
  cd QuoteGeneration
  tar zxvf %{SOURCE1}
)

%build

%if %{sgx_bootstrap} == 0
rm -rf prebuilt/openssl
rm -rf QuoteVerification/QVL/Src/ThirdParty

# Override PREPARE_SGXSSL since we have a sgx-ssl RPM already
# Override VERBOSE to avoid breaking driver/win/PLE/Makefile

# This ends up building the QuoteVerification and (part of) the
# tools/PCKCertSelection dirs too for some reason
%make_build -C QuoteGeneration \
    SGX_SDK=%{sgx_prefix} \
    SGX_ENCLAVE_SIGNER=%{sgx_sign} \
    SGX_EDGER8R=%{sgx_edger8r} \
    PREPARE_SGXSSL=/bin/true \
    ROOT_DIR="$(pwd)/QuoteGeneration" \
    SGXSSL_PACKAGE_PATH=%{sgx_prefix} \
    SGX_LIBRARY_PATH=%{sgx_libdir} \
    SGX_ENCLAVE_PATH=%{sgx_libdir}

%make_build -C QuoteVerification/dcap_tvl \
    -f Makefile.standalone \
    SGX_SDK=%{sgx_prefix} \
    SGX_ENCLAVE_SIGNER=%{sgx_sign} \
    SGX_EDGER8R=%{sgx_edger8r} \
    PREPARE_SGXSSL=/bin/true \
    COMMON_DIR="$(pwd)/QuoteGeneration/quote_wrapper/common" \
    DCAP_DIR="$(pwd)" \
    SGXSSL_PACKAGE_PATH=%{sgx_prefix} \
    SGX_LIBRARY_PATH=%{sgx_libdir}

for d in tools/PCKCertSelection tools/SGXPlatformRegistration tools/PCKRetrievalTool
do
  %make_build -C $d \
    SGX_SDK=%{sgx_prefix} \
    SGX_ENCLAVE_SIGNER=%{sgx_sign} \
    SGX_EDGER8R=%{sgx_edger8r} \
    PREPARE_SGXSSL=/bin/true \
    SGXSSL_PACKAGE_PATH=%{sgx_prefix} \
    SGX_LIBRARY_PATH=%{sgx_libdir} \
    SGX_ENCLAVE_PATH=%{sgx_libdir} \
    -j1
done

%make_build -C QuoteGeneration/quote_wrapper/quote/enclave/linux \
  SGX_SDK=%{sgx_prefix} \
  SGX_EDGER8R=%{sgx_edger8r} \
  SGX_LIBRARY_PATH=%{sgx_libdir} \
  COMMON_DIR="$(pwd)/QuoteGeneration/common" \
  qe3.so

%make_build -C QuoteGeneration/quote_wrapper/quote/id_enclave/linux \
  SGX_SDK=%{sgx_prefix} \
  SGX_EDGER8R=%{sgx_edger8r} \
  SGX_LIBRARY_PATH=%{sgx_libdir} \
  COMMON_DIR="$(pwd)/QuoteGeneration/common" \
  id_enclave.so

%make_build -C QuoteGeneration/quote_wrapper/tdx_quote/enclave/linux \
  SGX_SDK=%{sgx_prefix} \
  SGX_EDGER8R=%{sgx_edger8r} \
  SGX_LIBRARY_PATH=%{sgx_libdir} \
  COMMON_DIR="$(pwd)/QuoteGeneration/common" \
  tdqe.so

mkdir QuoteGeneration/pccs/lib
cp ./tools/PCKCertSelection/out/libPCKCertSelection.so QuoteGeneration/pccs/lib


for f in $(find -name createTarball.sh)
do
  $f
done

%endif

%install

%if %{sgx_bootstrap} == 0
%__install -d %{buildroot}%{_unitdir}
%__install -d %{buildroot}%{_bindir}
%__install -d %{buildroot}%{_sbindir}
%__install -d %{buildroot}%{_libdir}
%__install -d %{buildroot}%{_libexecdir}
%__install -d %{buildroot}%{_datadir}
%__install -d %{buildroot}%{_includedir}
%__install -d %{buildroot}%{_sysconfdir}
%__install -d %{buildroot}%{_sysusersdir}
%__install -d %{buildroot}%{_localstatedir}
%__install -d %{buildroot}%{sgx_libdir}
%endif

%__install -d %{buildroot}%{sgx_includedir}

cp QuoteGeneration/pce_wrapper/inc/sgx_pce.h \
  %{buildroot}%{sgx_includedir}/
cp QuoteGeneration/quote_wrapper/common/inc/sgx_quote_{3,4,5}.h \
  %{buildroot}%{sgx_includedir}/
cp QuoteGeneration/quote_wrapper/common/inc/sgx_ql_lib_common.h \
  %{buildroot}%{sgx_includedir}/
cp QuoteGeneration/quote_wrapper/common/inc/sgx_ql_quote.h \
  %{buildroot}%{sgx_includedir}/
cp QuoteGeneration/quote_wrapper/ql/inc/sgx_dcap_ql_wrapper.h \
  %{buildroot}%{sgx_includedir}/
cp QuoteGeneration/quote_wrapper/tdx_attest/tdx_attest.h \
  %{buildroot}%{sgx_includedir}/

cp QuoteVerification/dcap_tvl/sgx_dcap_tvl.edl \
   %{buildroot}%{sgx_includedir}/
cp QuoteVerification/dcap_tvl/sgx_dcap_tvl.h \
   %{buildroot}%{sgx_includedir}/
cp QuoteVerification/QvE/Include/sgx_qve_header.h \
  %{buildroot}%{sgx_includedir}/
cp QuoteVerification/dcap_quoteverify/inc/sgx_dcap_quoteverify.h \
  %{buildroot}%{sgx_includedir}/


%if %{sgx_bootstrap} == 0
# The SGX build process has no 'make install', it just
# bundles built files into tarballs mapping to packages
# designed for Debian packaging guidelines and then
# copied into RPM too. We extract all the tarballs and
# then have to re-arrange all the files to match how
# we want things installed and split between packages
for f in $(find -name *.tar.gz)
do
  gunzip -c $f | (cd %{buildroot} && tar xv)
done

for f in Makefile installConfig startup.sh cleanup.sh
do
  find %{buildroot} -name $f -delete
done

%__install -d %{buildroot}%{_sysconfdir}/PCKIDRetrievalTool

### Pre-built signed enclaves

for i in %{buildroot}/lib/*signed.so
do
   f=`basename $i`
   mv $i %{buildroot}%{sgx_libdir}/$f.1
done

### Host PCCS service

mv %{buildroot}/pkgroot/sgx-dcap-pccs/lib/libPCKCertSelection.so \
   %{buildroot}%{_libdir}/libPCKCertSelection.so
rm -f %{buildroot}/pkgroot/sgx-dcap-pccs/pccs.service
mv %{buildroot}/pkgroot/sgx-dcap-pccs \
   %{buildroot}%{_datadir}/pccs

(
    cd %{buildroot}%{_datadir}/pccs
    tar Jxvf %{SOURCE7}
    # Keep brp-mangle-shebangs happy
    find node_modules -type f -exec chmod -x {} \;
    perl -i -p -e 's,/usr/bin/env python,/usr/bin/env python3,' node_modules/ffi-napi/deps/libffi/generate-darwin-source-and-headers.py

    rm -f install.sh README.md
)

cp %{SOURCE8} %{buildroot}%{_libexecdir}/pccs-ssl
chmod +x %{buildroot}%{_libexecdir}/pccs-ssl

cat >>%{buildroot}%{_bindir}/pccs <<EOF
#!/usr/bin/sh

exec node /usr/share/pccs/pccs_server.js
EOF
chmod +x %{buildroot}%{_bindir}/pccs

# Home dir for 'pccs' user
%__install -d %{buildroot}%{_sharedstatedir}/pccs

%__install -m 0644 %{SOURCE2} %{buildroot}%{_sysusersdir}/pccs.conf
%__install -m 0644 %{SOURCE3} %{buildroot}%{_unitdir}/pccs.service

# XXX bundle nodejs modules

%__install -d %{buildroot}%{_localstatedir}/log/pccs
%__install -d %{buildroot}%{_sysconfdir}/pccs
%__install -d %{buildroot}%{_sysconfdir}/pccs/ssl

mv %{buildroot}%{_datadir}/pccs/config/default.json \
  %{buildroot}%{_sysconfdir}/pccs
rmdir %{buildroot}%{_datadir}/pccs/config


### Host PCCS admin tool

%__install -d %{buildroot}%{_datadir}/pccsadmin
cp tools/PccsAdminTool/pccsadmin.py %{buildroot}%{_datadir}/pccsadmin/pccsadmin.py
cp -a tools/PccsAdminTool/lib %{buildroot}%{_datadir}/pccsadmin/lib

cat > %{buildroot}%{_bindir}/pccsadmin <<EOF
#!/bin/sh

exec python3 %{_datadir}/pccsadmin/pccsadmin.py "\$@"
EOF
chmod +x %{buildroot}%{_bindir}/pccsadmin

### Host PCK ID tool

# XXX must patch source to look in sysconfdir
mv %{buildroot}/pkgroot/sgx-pck-id-retrieval-tool/PCKIDRetrievalTool \
   %{buildroot}%{_bindir}/
mv %{buildroot}/pkgroot/sgx-pck-id-retrieval-tool/network_setting.conf \
   %{buildroot}%{_sysconfdir}/PCKIDRetrievalTool/network_setting.conf
rm -f %{buildroot}/pkgroot/sgx-pck-id-retrieval-tool/License.txt
rm -f %{buildroot}/pkgroot/sgx-pck-id-retrieval-tool/README.txt
rmdir %{buildroot}/pkgroot/sgx-pck-id-retrieval-tool


### Registration agent...

mv %{buildroot}/pkgroot/sgx-ra-service/mpa_manage \
   %{buildroot}%{_bindir}/mpa_manage
mv %{buildroot}/pkgroot/sgx-ra-service/mpa_registration \
   %{buildroot}%{_sbindir}/mpa_registration
mv %{buildroot}/pkgroot/sgx-ra-service/conf/mpa_registration.conf \
   %{buildroot}%{_sysconfdir}/mpa_registration.conf
rm -f %{buildroot}/pkgroot/sgx-ra-service/mpa_registration_tool.conf
rm -f %{buildroot}/pkgroot/sgx-ra-service/mpa_registration_tool.service
rmdir %{buildroot}/pkgroot/sgx-ra-service/conf
rmdir %{buildroot}/pkgroot/sgx-ra-service

%__install -m 0644 %{SOURCE6} %{buildroot}%{_unitdir}/mpa_registration.service


## ...and its network addon

mv %{buildroot}/pkgroot/libsgx-ra-network-dev/include/*.h \
   %{buildroot}%{_includedir}/
rmdir %{buildroot}/pkgroot/libsgx-ra-network-dev/include
rmdir %{buildroot}/pkgroot/libsgx-ra-network-dev
mv %{buildroot}/pkgroot/libsgx-ra-network/lib/*.so \
   %{buildroot}%{_libdir}/
rmdir %{buildroot}/pkgroot/libsgx-ra-network/lib
rmdir %{buildroot}/pkgroot/libsgx-ra-network

## ...and its uefi addon

mv %{buildroot}/pkgroot/libsgx-ra-uefi-dev/include/*.h \
   %{buildroot}%{_includedir}/
rmdir %{buildroot}/pkgroot/libsgx-ra-uefi-dev/include
rmdir %{buildroot}/pkgroot/libsgx-ra-uefi-dev
mv %{buildroot}/pkgroot/libsgx-ra-uefi/lib/*.so \
   %{buildroot}%{_libdir}/
rmdir %{buildroot}/pkgroot/libsgx-ra-uefi/lib
rmdir %{buildroot}/pkgroot/libsgx-ra-uefi


# Host quoting libraries
mv %{buildroot}/pkgroot/libsgx-dcap-default-qpl-dev/include/sgx_default_quote_provider.h \
   %{buildroot}%{_includedir}/
rmdir %{buildroot}/pkgroot/libsgx-dcap-default-qpl-dev/include
rmdir %{buildroot}/pkgroot/libsgx-dcap-default-qpl-dev

mv %{buildroot}/pkgroot/libsgx-dcap-default-qpl/etc/sgx_default_qcnl.conf \
   %{buildroot}%{_sysconfdir}/
mv %{buildroot}/pkgroot/libsgx-dcap-default-qpl/lib/libdcap_quoteprov.so \
   %{buildroot}%{_libdir}/
mv %{buildroot}/pkgroot/libsgx-dcap-default-qpl/lib/libsgx_default_qcnl_wrapper.so \
   %{buildroot}%{_libdir}/
rmdir %{buildroot}/pkgroot/libsgx-dcap-default-qpl/etc
rmdir %{buildroot}/pkgroot/libsgx-dcap-default-qpl/lib
rmdir %{buildroot}/pkgroot/libsgx-dcap-default-qpl

mv %{buildroot}/pkgroot/libsgx-dcap-ql-dev/include/sgx_dcap_ql_wrapper.h \
   %{buildroot}%{_includedir}/
rmdir %{buildroot}/pkgroot/libsgx-dcap-ql-dev/include
rmdir %{buildroot}/pkgroot/libsgx-dcap-ql-dev

mv %{buildroot}/pkgroot/libsgx-dcap-ql/lib/libsgx_dcap_ql.so \
   %{buildroot}%{_libdir}/
rmdir %{buildroot}/pkgroot/libsgx-dcap-ql/lib
rmdir %{buildroot}/pkgroot/libsgx-dcap-ql

mv %{buildroot}/pkgroot/libsgx-dcap-quote-verify-dev/include/sgx_dcap_quoteverify.h \
   %{buildroot}%{_includedir}/
mv %{buildroot}/pkgroot/libsgx-dcap-quote-verify-dev/include/sgx_qve_header.h \
   %{buildroot}%{_includedir}/
rmdir %{buildroot}/pkgroot/libsgx-dcap-quote-verify-dev/include
rmdir %{buildroot}/pkgroot/libsgx-dcap-quote-verify-dev

mv %{buildroot}/pkgroot/libsgx-dcap-quote-verify/lib/libsgx_dcap_quoteverify.so \
   %{buildroot}%{_libdir}/
rmdir %{buildroot}/pkgroot/libsgx-dcap-quote-verify/lib
rmdir %{buildroot}/pkgroot/libsgx-dcap-quote-verify

mv %{buildroot}/pkgroot/libsgx-pce-logic/lib/libsgx_pce_logic.so \
   %{buildroot}%{_libdir}/
rmdir %{buildroot}/pkgroot/libsgx-pce-logic/lib
rmdir %{buildroot}/pkgroot/libsgx-pce-logic

mv %{buildroot}/pkgroot/libsgx-qe3-logic/lib/libsgx_qe3_logic.so \
   %{buildroot}%{_libdir}/
rmdir %{buildroot}/pkgroot/libsgx-qe3-logic/lib
rmdir %{buildroot}/pkgroot/libsgx-qe3-logic



### TDX guest attestation library

# XXX versioned symlinks
mv %{buildroot}/pkgroot/libtdx-attest/lib/libtdx_attest.so \
   %{buildroot}%{_libdir}/
rmdir %{buildroot}/pkgroot/libtdx-attest/lib
rmdir %{buildroot}/pkgroot/libtdx-attest

mv %{buildroot}/pkgroot/libtdx-attest-dev/include/tdx_attest.h \
   %{buildroot}%{_includedir}/
rmdir %{buildroot}/pkgroot/libtdx-attest-dev/include
rm %{buildroot}/pkgroot/libtdx-attest-dev/sample/test_tdx_attest.c
rmdir %{buildroot}/pkgroot/libtdx-attest-dev/sample
rmdir %{buildroot}/pkgroot/libtdx-attest-dev


### TDX platform quote generation service

mv %{buildroot}/pkgroot/tdx-qgs/conf/qgs.conf \
   %{buildroot}%{_sysconfdir}/qgs.conf
mv %{buildroot}/pkgroot/tdx-qgs/qgs \
   %{buildroot}%{_sbindir}/qgs

# Switch from vsock to unix socket to avoid exposing it
# to all VMs unconditionally
sed -i -e 's/^port/#port/' %{buildroot}%{_sysconfdir}/qgs.conf

rm -f %{buildroot}/pkgroot/tdx-qgs/linksgx.sh
rm -f %{buildroot}/pkgroot/tdx-qgs/qgsd.conf
rm -f %{buildroot}/pkgroot/tdx-qgs/qgsd.service
rmdir %{buildroot}/pkgroot/tdx-qgs/conf
rmdir %{buildroot}/pkgroot/tdx-qgs

# Home dir for 'qgs' user
%__install -d %{buildroot}%{_sharedstatedir}/qgs
# XXX patch source to just 'qgs' instead of 'tdx-qgs' ?
%__install -d %{buildroot}%{_rundir}/tdx-qgs

%__install -m 0644 %{SOURCE4} %{buildroot}%{_sysusersdir}/qgs.conf
%__install -m 0644 %{SOURCE5} %{buildroot}%{_unitdir}/qgs.service


### TDX platform libraries

mv %{buildroot}/pkgroot/libsgx-tdx-logic-dev/include/*.h \
   %{buildroot}%{_includedir}/
rmdir %{buildroot}/pkgroot/libsgx-tdx-logic-dev/include
rmdir %{buildroot}/pkgroot/libsgx-tdx-logic-dev

mv %{buildroot}/pkgroot/libsgx-tdx-logic/lib/*.so \
   %{buildroot}%{_libdir}/
rmdir %{buildroot}/pkgroot/libsgx-tdx-logic/lib
rmdir %{buildroot}/pkgroot/libsgx-tdx-logic

### License file no needed in this context

rm -rf %{buildroot}/package

cp QuoteVerification/dcap_tvl/libsgx_dcap_tvl.a \
   %{buildroot}%{sgx_libdir}/

cp tools/PCKCertSelection/include/pck_cert_selection.h \
   %{buildroot}%{_includedir}/

cp QuoteGeneration/quote_wrapper/quote/inc/sgx_ql_core_wrapper.h \
   %{buildroot}%{_includedir}/

### Enclaves (unsigned)

cp QuoteGeneration/quote_wrapper/quote/enclave/linux/qe3.so \
   %{buildroot}%{sgx_libdir}/libsgx_qe3.so
cp QuoteGeneration/quote_wrapper/quote/id_enclave/linux/id_enclave.so \
   %{buildroot}%{sgx_libdir}/libsgx_id_enclave.so
cp QuoteGeneration/quote_wrapper/tdx_quote/enclave/linux/tdqe.so \
   %{buildroot}%{sgx_libdir}/libsgx_tdqe.so
cp QuoteGeneration/quote_wrapper/tdx_quote/enclave/linux/tdqe.so \
   %{buildroot}%{sgx_libdir}/libsgx_tdqe.so

cp QuoteVerification/QvE/qve.so \
   %{buildroot}%{sgx_libdir}/libsgx_qve.so

%endif

%pre -n sgx-platform-pccs
%sysusers_create_compat %{SOURCE2}

%post -n sgx-platform-pccs
%systemd_post pccs.service

%preun -n sgx-platform-pccs
%systemd_preun pccs.service

%postun -n sgx-platform-pccs
%systemd_postun_with_restart pccs.service


%post -n sgx-platform-mp-ra
%systemd_post mpa_registration.service

%preun -n sgx-platform-mp-ra
%systemd_preun mpa_registration.service

%postun -n sgx-platform-mp-ra
%systemd_postun_with_restart mpa_registration.service


%pre -n tdx-platform-qgs
%sysusers_create_compat %{SOURCE4}

%post -n tdx-platform-qgs
%systemd_post qgs.service

%preun -n tdx-platform-qgs
%systemd_preun qgs.service

%postun -n tdx-platform-qgs
%systemd_postun_with_restart qgs.service


%if %{sgx_bootstrap} == 0
%files -n sgx-platform-dcap-libs
# XXX other sub-rpms need this ?
%license License.txt
%config(noreplace) %{_sysconfdir}/sgx_default_qcnl.conf
%{_libdir}/libdcap_quoteprov.so
%{_libdir}/libdcap_quoteprov.so.1
%{_libdir}/libsgx_dcap_ql.so
%{_libdir}/libsgx_dcap_ql.so.1
%{_libdir}/libsgx_dcap_quoteverify.so
%{_libdir}/libsgx_dcap_quoteverify.so.1
%{_libdir}/libsgx_default_qcnl_wrapper.so
%{_libdir}/libsgx_default_qcnl_wrapper.so.1
%{_libdir}/libsgx_pce_logic.so
%{_libdir}/libsgx_pce_logic.so.1
%{_libdir}/libsgx_qe3_logic.so
%{_libdir}/libPCKCertSelection.so
%{_libdir}/libPCKCertSelection.so.1

%files -n sgx-platform-dcap-devel
%{_includedir}/sgx_dcap_ql_wrapper.h
%{_includedir}/sgx_dcap_quoteverify.h
%{_includedir}/sgx_default_quote_provider.h
%{_includedir}/sgx_qve_header.h
%{_includedir}/pck_cert_selection.h
%{_includedir}/sgx_ql_core_wrapper.h

%{_includedir}/mp_network.h
%{_includedir}/MPNetwork.h
%{_includedir}/MPNetworkDefs.h

%{_includedir}/mp_uefi.h
%{_includedir}/MultiPackageDefs.h
%{_includedir}/MPUefi.h


%files -n sgx-platform-pccs
%{_bindir}/pccs
%{_libexecdir}/pccs-ssl
%dir %{_sysconfdir}/pccs
%attr(0750,root,pccs) %dir %{_sysconfdir}/pccs/ssl
%config(noreplace) %{_sysconfdir}/pccs/default.json
%{_unitdir}/pccs.service
%{_datadir}/pccs
%{_sysusersdir}/pccs.conf
%attr(0700,pccs,pccs) %dir %{_sharedstatedir}/pccs
%attr(0700,pccs,pccs) %dir %{_localstatedir}/log/pccs


%files -n sgx-platform-pccs-admin
%{_bindir}/pccsadmin
%{_datadir}/pccsadmin


%files -n sgx-platform-pck-tool
%license tools/PCKRetrievalTool/License.txt
%doc tools/PCKRetrievalTool/README_standalone.txt
%dir %{_sysconfdir}/PCKIDRetrievalTool
%config(noreplace) %{_sysconfdir}/PCKIDRetrievalTool/network_setting.conf
%{_bindir}/PCKIDRetrievalTool


%files -n sgx-platform-mp-ra
%{_bindir}/mpa_manage
%{_sbindir}/mpa_registration
%{_unitdir}/mpa_registration.service
%config(noreplace) %{_sysconfdir}/mpa_registration.conf

%files -n sgx-platform-mp-libs
%{_libdir}/libmpa_network.so
%{_libdir}/libmpa_network.so.1
%{_libdir}/libmpa_uefi.so
%{_libdir}/libmpa_uefi.so.1
%endif

%files -n sgx-enclave-dcap-devel
%{sgx_includedir}/sgx_pce.h
%{sgx_includedir}/sgx_quote_3.h
%{sgx_includedir}/sgx_quote_4.h
%{sgx_includedir}/sgx_quote_5.h
%{sgx_includedir}/sgx_qve_header.h
%{sgx_includedir}/sgx_ql_lib_common.h
%{sgx_includedir}/sgx_ql_quote.h
%{sgx_includedir}/sgx_dcap_quoteverify.h
%{sgx_includedir}/sgx_dcap_ql_wrapper.h
%{sgx_includedir}/sgx_dcap_tvl.h
%{sgx_includedir}/sgx_dcap_tvl.edl
%{sgx_includedir}/tdx_attest.h

%if %{sgx_bootstrap} == 0
%{sgx_libdir}/libsgx_dcap_tvl.a


%files -n sgx-enclave-dcap-unsigned
%{sgx_libdir}/libsgx_qe3.so
%{sgx_libdir}/libsgx_qve.so
%{sgx_libdir}/libsgx_id_enclave.so


%files -n sgx-enclave-dcap-signed
%{sgx_libdir}/libsgx_qe3.signed.so.1
%{sgx_libdir}/libsgx_qve.signed.so.1
%{sgx_libdir}/libsgx_id_enclave.signed.so.1


%files -n tdx-platform-qgs
%{_sbindir}/qgs
%{_unitdir}/qgs.service
%config(noreplace) %{_sysconfdir}/qgs.conf
%{_sysusersdir}/qgs.conf
%attr(0700,qgs,qgs) %dir %{_sharedstatedir}/qgs
%attr(0700,qgs,qgs) %dir %{_rundir}/tdx-qgs

%files -n tdx-platform-libs
%{_libdir}/libsgx_tdx_logic.so
%{_libdir}/libsgx_tdx_logic.so.1


%files -n tdx-platform-devel
%{_includedir}/td_ql_wrapper.h
# XXX libs


%files -n tdx-enclave-unsigned
%{sgx_libdir}/libsgx_tdqe.so


%files -n tdx-enclave-signed
%{sgx_libdir}/libsgx_tdqe.signed.so.1


%files -n tdx-guest-libs
# XXX versioned links
%{_libdir}/libtdx_attest.so
%{_libdir}/libtdx_attest.so.1


%files -n tdx-guest-devel
%{_includedir}/tdx_attest.h
# XXX libs
%endif


%changelog
* Mon Jan 29 2024 Daniel P. Berrangé <berrange@redhat.com> - 1.19-1
- Initial packaging
