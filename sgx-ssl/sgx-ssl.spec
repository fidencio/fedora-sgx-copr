# This package only provides SGX enclave content
%define debug_package %{nil}

# We need direct control over build flags used for the
# SGX enclave code.
%undefine _auto_set_build_flags

# Set to '1' if rebasing to a new version and do a full
# bootstrap of the SGX enclave stack
%{!?sgx_bootstrap:%define sgx_bootstrap 0}

Name: sgx-ssl
Version: 3.0_Rev1
Release: 1%{?dist}
Summary: SGX support for SSL 
License: Apache-2.0 AND BSD-3-Clause AND 

Source0: https://github.com/intel/intel-sgx-ssl/archive/refs/tags/%{version}.tar.gz#/intel-sgx-ssl-%{version}.tar.gz

Source1: https://www.openssl.org/source/openssl-3.0.10.tar.gz
Provides: bundled(openssl) = 3.0.10

Patch1: 0001-Disable-build-of-test-app.patch
Patch2: 0002-Disable-various-EC-crypto-features.patch
Patch3: 0003-Disable-sm2-and-sm4-crypto-algorithms.patch

BuildRequires: sgx-srpm-macros
BuildRequires: sgx-enclave-devel
BuildRequires: make
BuildRequires: gcc
BuildRequires: gcc-c++
BuildRequires: perl-base
BuildRequires: perl(FindBin)
BuildRequires: perl(lib)
BuildRequires: perl(IPC::Cmd)
BuildRequires: perl(File::Compare)
BuildRequires: perl(File::Copy)

%description
The SGX SSL cryptographic library is intended to provide
support for enclave development

%package -n sgx-enclave-ssl-devel
Summary: SGX support for SSL development libraries and headers

Requires: sgx-enclave-devel

%description -n sgx-enclave-ssl-devel
The SGX SSL cryptographic library is intended to provide
support for enclave development

This package provides the development libraries and header files

%prep
%autosetup -p1 -n intel-sgx-ssl-%{version}

cp %{SOURCE1} openssl_source/

%build

%make_build -C Linux/sgx \
   SGX_SDK=%{sgx_prefix} \
   SGX_ENCLAVE_SIGNER=%{sgx_sign} \
   SGX_EDGER8R=%{sgx_edger8r} \
   MITIGATION_CVE_2020_0551=%{sgx_cve_2020_0551_mitigation} \
   MITIGATION_CFLAGS="%{sgx_cve_2020_0551_mitigation_cflags}"

%install

%__install -d %{buildroot}%{sgx_libdir}
%__install -d %{buildroot}%{sgx_includedir}

cp -a Linux/package/lib64/*  %{buildroot}%{sgx_libdir}/
cp -a Linux/package/include/*  %{buildroot}%{sgx_includedir}/

%files -n sgx-enclave-ssl-devel
%license License.txt
%{sgx_libdir}/libsgx_tsgxssl.a
%{sgx_libdir}/libsgx_tsgxssl_crypto.a
%{sgx_libdir}/libsgx_usgxssl.a

%dir %{sgx_includedir}/openssl/
%{sgx_includedir}/openssl/*
%{sgx_includedir}/sgx_tsgxssl.edl
%{sgx_includedir}/tSgxSSL_api.h
%{sgx_includedir}/tsgxsslio.h

%changelog
* Mon Jan 29 2024 Daniel P. Berrangé <berrange@redhat.com> - 3.0_Rev1-1
- Initial package
